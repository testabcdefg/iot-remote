package com.bootdo.point.service;

import com.bootdo.point.domain.HisDataDO;

import java.util.List;
import java.util.Map;

/**
 * 实时数据
 * 
 * @author langxianwei
 * @email 1992lcg@163.com
 * @date 2018-11-25 18:42:21
 */
public interface HisDataService {
	
	HisDataDO get(Integer id);
	
	List<HisDataDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(HisDataDO hisData);
	
	int update(HisDataDO hisData);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}
